CREATE TABLE Address(
    id INT NOT NULL AUTO_INCREMENT,
    street VARCHAR(20) NOT NULL,
    city VARCHAR(20) NOT NULL,
    province VARCHAR(20) NOT NULL,
    country VARCHAR(20) NOT NULL,
    zipCode VARCHAR(20) NOT NULL,
    PRIMARY KEY (Id)  
);

CREATE TABLE Brand(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    PRIMARY KEY (Id)  
);


CREATE TABLE Valuta(
    id INT NOT NULL AUTO_INCREMENT,
    code VARCHAR(20) NOT NULL,
    name VARCHAR(20) NOT NULL,
    PRIMARY KEY (Id)  
);


CREATE TABLE Manufacturer(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    addressId INT NOT NULL,
    PRIMARY KEY (Id),
    FOREIGN KEY (addressId) REFERENCES Address(id)
    ON DELETE CASCADE  
    ON UPDATE CASCADE  
);

CREATE TABLE Product(
    id INT NOT NULL AUTO_INCREMENT,
    artNumber INT NOT NULL,
    name VARCHAR(20) NOT NULL,
    description VARCHAR(20) NOT NULL,
    manufacturerId INT NOT NULL,
    brandId INT NOT NULL,
    stock INT NOT NULL,
    PRIMARY KEY (Id),
    FOREIGN KEY (manufacturerId) REFERENCES Manufacturer(id),
    FOREIGN KEY (brandId) REFERENCES Brand(id)
    ON DELETE CASCADE  
    ON UPDATE CASCADE  
);



CREATE TABLE Price(
    id INT NOT NULL AUTO_INCREMENT,
    productId INT NOT NULL,
    valutaId INT NOT NULL,
    amount INT NOT NULL,
    PRIMARY KEY (Id),
    FOREIGN KEY (productId) REFERENCES Product(id),
    FOREIGN KEY (valutaId) REFERENCES Valuta(id)
    ON DELETE CASCADE  
	ON UPDATE CASCADE  
);


