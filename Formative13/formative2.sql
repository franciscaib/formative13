INSERT INTO Address(Street, city, province, country, zipCode) VALUES  
('Paingan', 'Sleman', 'DIYogyakarta', 'Indonesia', '55282'),  
('Armada', 'Cilacap','Jawa Tengah', 'Indonesia', '53271');

INSERT INTO Brand(name) VALUES
('Indomie'),
('Silver Queen');

INSERT INTO Valuta(code, name) VALUES
('idr','Rupiah'),
('usd', 'US Dollar');

INSERT INTO Manufacturer(name, addressId) VALUES
('PT. Indofood', 4),
('PT. Petra Food', 5);

INSERT INTO Product(artNumber, name, description, manufacturerId, brandId, stock) VALUES
(11, 'Indomie goreng', 'Makanan Instan',3, 1, 50),
(12, 'Supermie', 'Makanan Instan',3, 1, 20),
(13, 'Sarimie', 'Makanan Instan',3, 1, 100),
(14, 'Popmie', 'Makanan Instan',3, 1, 10),
(15, 'Mi Sakura', 'Makanan Instan',3, 1, 0),
(16, 'SQ Chasew', 'Coklat',4, 2, 40),
(16, 'SQ Chunky Bar', 'Coklat',4, 2, 30),
(16, 'SQ Almond', 'Coklat',4, 2, 52),
(16, 'SQ Very Berry', 'Coklat',4, 2, 14),
(16, 'SQ Bites', 'Coklat',4, 2, 0);

INSERT INTO Price (productId, valutaId, amount) VALUES
(11, 1, 4000),
(11, 2, 5),
(12, 1, 3500),
(12, 2, 4),
(13, 1, 3500),
(13, 2, 4),
(14, 1, 5000),
(14, 2, 5),
(15, 1, 3500),
(15, 2, 4),
(16, 1, 10000),
(16, 2, 10),
(17, 1, 11000),
(17, 2, 11),
(18, 1, 13000),
(18, 2, 13),
(19, 1, 15000),
(19, 2, 15),
(20, 1, 8500),
(20, 2, 7);
