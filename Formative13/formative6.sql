SELECT valuta.code AS valuta_code, manufacturer.name AS Manufacturer, 
	brand.name AS Brand , address.province AS Address,
	valuta.name AS Valuta, product.name AS Product, 
    product.stock AS product_stock, price.amount AS harga
	FROM price 
	JOIN product ON product.id = price.productId 
	JOIN valuta ON valuta.id = price.valutaId
	JOIN  brand ON brand.id = product.brandId
	JOIN manufacturer ON manufacturer.id = product.manufacturerId
	JOIN Address ON Address.id = manufacturer.addressid
	WHERE product.stock = 0
	group by valuta.code
	order by harga DESC;